package com.userservice.UserService.controller;

import com.userservice.UserService.DTO.OrderDTO;
import com.userservice.UserService.DTO.userDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("api/user")

public class usercontroller{
    @Autowired
    private UserService userService;

    @GetMapping("/getAll")
    public List<userDTO> getAllUsers(){
        return UserService.getAllUsers();
    }
    /**
     * ==================================
     * this method used for return for all user order data
     * ===================================
     */

    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO>getOrdersByUserId(@PathVariable final Long id){
        return userService.getOrdersByUserId(id);
    }


}
